//
// Created by tomas on 15. 12. 2020.
//

#include "Game.h"

/* Game
 * Zavolání a spuštění samotné hry
 * Vytvoření hrdiny a přiřazení základního vybavení + přidělena začáteční lokace
 * Po vytvoření hrdiny se provede zavolání hlavního menu hry
 */

Game::Game() {
    std::cout << std::endl << "Vítej v naší adventuře Hollywood adventures." << std::endl << std::endl;
    m_hero = new Hero();
    m_hero->setStats();
    setLocations();
    m_hero->setLocation(m_locations.at(0));
    m_hero->learnInteraction(new Fight());
    std::cout << "Začínáš v lokaci: " << m_hero->getLocation()->getLocationName() << std::endl;
    std::cout << "Popis lokace: " << m_hero->getLocation()->getLocationDescription() << std::endl << std::endl;
    std::cout << "Jako začáteční brnění si dostal: " << m_hero->getArmor()->getArmorName() << std::endl << std::endl;
    std::cout << "Jako začáteční zbraň si dostal: " << m_hero->getWeapon()->getWeaponName() << std::endl << std::endl;
    m_hero->printInfo();
    std::cout << std::endl << "==============================" << std::endl;
    HeroOptions();
}

int Game::endGame() {
    std::cout << "Opravdu si přeješ ukončit hru?" << std::endl << "[Y/N]" << std::endl<<"> "<<std::endl;
    char options;
    std::cin>>options;
    switch (options) {
        case 'Y':
            std::cout << "Ukončil jsi hru";
            return 0;
        case 'N':
            HeroOptions();
            break;
        default:
            std::cout << "Zadal jsi špatný znak" << std::endl;
            endGame();
            break;
    }
    return 0;
}


//vytvoření lokací
void Game::setLocations() {
    auto locationFactory = new LocationFactory();
    m_locations.push_back(locationFactory->getLocationHollywood());
    m_locations.push_back(locationFactory->getLocationVenice());
    m_locations.push_back(locationFactory->getLocationTheObservatory());
    delete locationFactory;
}

/* HeroOptions
 * Hlavní menu hry
 * Uživatel zde může dělat svoje rozhodnutí, zda chce cestovat, vypsat info atd.
*/

int Game::HeroOptions() {
    char options;
    std::cout << std::endl << "Možnosti: " << std::endl;
    std::cout << "[1] Cestovat " << std::endl;
    std::cout << "[2] Prozkoumat lokaci " << std::endl;
    std::cout << "[3] Mapa " << std::endl;
    std::cout << "[4] Informace o hrdinovi " << std::endl;
    std::cout << "[5] Informace o vybavení " << std::endl;
    std::cout << "[6] Zobrazit inventář " << std::endl;
    std::cout << "[7] Interaguj s nepřáteli v lokaci" << std::endl;
    std::cout << "[8] Ukončit hru" << std::endl;
    std::cout << "> ";
    std::cin >> options;
    switch (options) {
        case '1':
            option1();
        case '2':
            option2();
        case '3':
            option3();
        case '4':
            option4();
        case '5':
            option5();
        case '6':
            option6();
        case '7':
            option7();
        case '8':
            endGame();
            break;
        default:
            std::cout << std::endl << "Číslo volby není na seznamu."<<std::endl;
            HeroOptions();
    }
    return 0;
}

//funkce na cestování
void Game::option1() {
    printMap();
    std::cout << std::endl;
    std::cout << "[z]pět  [d]opředu" << std::endl;
    std::cout << "> ";
    changeLocation();
    HeroOptions();
}

//funkce na prozkoumání lokace
void Game::option2() {
    std::cout << "==============================" << std::endl;
    printEnemies();
    std::cout << "==============================" << std::endl;
    HeroOptions();
}

//funkce pro zobrazení mapy
void Game::option3() {
    printMap();
    std::cout << "Popis lokace: " << m_hero->getLocation()->getLocationDescription() << std::endl;
    std::cout << "==============================" << std::endl;
    HeroOptions();
}

//funkce pro výpis informací o hrdinovi
void Game::option4() {
    std::cout << std::endl;
    std::cout << "==============================" << std::endl;
    m_hero->printInfo();
    std::cout << "==============================" << std::endl;
    HeroOptions();
}

//funkce pro výpis nasazeného vybavení
void Game::option5() {
    std::cout << std::endl;
    std::cout << "==============================" << std::endl;
    m_hero->printEquipment();
    std::cout << "==============================" << std::endl;
    HeroOptions();
}

//funkce pro zobrazení inventáře
void Game::option6() {
    std::cout << std::endl;
    m_hero->printInventory();
    inventoryOptions();
    HeroOptions();
}

//funkce na interakci s enemy
void Game::option7() {
    int decision = 0;
    std::cout << std::endl;
    std::cout << "==============================" << std::endl;
    std::cout << "S kým si přeješ bojovat?" << std::endl;
    std::cout << "==============================" << std::endl;
    std::cout<<"[0] Zpět"<<std::endl;
    for (int i = 0; i < m_hero->getLocation()->getEnemies().size(); ++i) {
        std::cout << "[" << i+1 << "] " << m_hero->getLocation()->getEnemies().at(i)->getName();
        if (m_hero->getLocation()->getEnemies().at(i)->getStatus() == true)
            std::cout<<" - poražen" <<std::endl;
        else std::cout<<std::endl;}
    std::cout << "[" << m_hero->getLocation()->getEnemies().size()+1<< "] "
              << m_hero->getLocation()->getBoss()->getName();
    if (m_hero->getLocation()->getBoss()->getStatus() == true)
        std::cout<<" - poražen"<<std::endl;
    else std::cout<<std::endl;
    std::cout << "==============================" << std::endl;
    std::cout << "> ";
    std::cin >> decision;
    decision += -1;
    if (decision == -1)
        HeroOptions();
    else {
        if (decision <= (m_hero->getLocation()->getEnemies().size()) and decision >= 0) {
            if (decision < m_hero->getLocation()->getEnemies().size()) {
                if (m_hero->getLocation()->getEnemies().at(decision)->getStatus() == false) {
                    m_hero->interactEnemy(m_hero->getLocation()->getEnemies().at(decision));
                } else
                    std::cout << "Nepřítel byl poražen" << std::endl;
            }
            if (decision == m_hero->getLocation()->getEnemies().size()) {
                bool allDead = true;
                for (int i = 0; i < m_hero->getLocation()->getEnemies().size(); ++i) {
                    if (m_hero->getLocation()->getEnemies().at(i)->getStatus() == false)
                        allDead = false;
                }
                if (allDead == true) {
                    if (m_hero->getLocation()->getBoss()->getStatus() == false)
                        m_hero->interactBoss(m_hero->getLocation()->getBoss());
                    else std::cout << "Boss už byl poražen" << std::endl;
                } else {
                    std::cout << "Nejdříve musíš porazit všechny základní nepřátele" << std::endl;
                }
            }
        } else {
            std::cout << "Zadal si číslo, které není na seznamu." << std::endl;
            option7();
        }
        HeroOptions();
    }
}

//funkce pro výběr předmětů z inventáře
void Game::inventoryOptions() {
    std::cout<<"Možnosti: "<<std::endl;
    std::cout<<"[0] Zpět"<<std::endl;
    std::cout<<"[1] Nasadit zbraň"<<std::endl;
    std::cout<<"[2] Nasadit brnění"<<std::endl;
    std::cout<<"[3] Vyhodit zbraň"<<std::endl;
    std::cout<<"[4] Vyhodit brnění"<<std::endl;
    std::cout<<"[5] Použít Léčivý nápoj"<<std::endl;
    std::cout<<"[6] Použít RedBull"<<std::endl;
    char options;
    std::cout << "> ";
    std::cin >> options;
    switch (options) {
        case '1':
            m_hero -> equipWeapon(m_hero->getInventory()->selectWeapon());
            m_hero->printInventory();
            inventoryOptions();
            break;
        case '2':
            m_hero -> equipArmor(m_hero->getInventory()->selectArmor());
            m_hero->printInventory();
            inventoryOptions();
            break;
        case '3':
            m_hero ->getInventory()->putWeaponOut(m_hero->getInventory()->selectWeapon());
            m_hero->printInventory();
            inventoryOptions();
            break;
        case '4':
            m_hero -> getInventory()->putArmorOut(m_hero->getInventory()->selectArmor());
            m_hero->printInventory();
            inventoryOptions();
            break;
        case '5':
            m_hero->useHealthPotion();
            m_hero->printInventory();
            inventoryOptions();
            break;
        case '6':
            m_hero->useStaminaPotion();
            m_hero->printInventory();
            inventoryOptions();
            break;
        case '0':
            HeroOptions();
        default:
            std::cout<<"Špatný input!"<<std::endl;
            inventoryOptions();
            break;
    }
}

//funkce na pohyb po mape, posouva se vzdy o jedna zpet nebo o jedno dopredu
void Game::changeLocation() {
    char options;
    std::cin >> options;
    switch (options) {
        case 'z':
            if (m_hero->getLocation() != m_locations.at(0)) {
                m_hero->setLocation(m_locations.at(m_hero->getMapPosition() - 1));
                m_hero->setMapPosition(-1);
                std::cout << "Přesunul ses na lokaci: " << m_hero->getLocation()->getLocationName() << std::endl
                          << std::endl;
            } else
                std::cout << "Nelze jít zpět" << std::endl;
            break;
        case 'd':
            if (m_hero->getKeys() > m_hero->getMapPosition()) {
            if (m_hero->getMapPosition() != 2) {
                    m_hero->setLocation(m_locations.at(m_hero->getMapPosition() + 1));
                    m_hero->setMapPosition(1);
                    std::cout << "Přesunul ses na lokaci: " << m_hero->getLocation()->getLocationName() << std::endl
                              << std::endl;
                } else
                    std::cout << "Nelze jít dopředu: " << std::endl;
            }else std::cout<<"Nemáš klíč do další lokace"<<std::endl<<"Aby si ho získal, musíš porazit Bosse v současné lokaci"<<std::endl;
            break;
        default:
            std::cout << "Zadal jsi špatný příkaz, tvoje lokace zůstává stejná" << std::endl;
            break;
    }
}

//funkce na vytisteni mapy
void Game::printMap() {
    std::cout << std::endl << "             Mapa" << std::endl;
    std::cout << "==============================" << std::endl;
    for (int i = 0; i < m_locations.size(); ++i) {
        std::cout << m_locations.at(i)->getLocationName();
        if (m_hero->getLocation() == m_locations.at(i))
            std::cout << " <<< zde se nacházíš" << std::endl;
        else
            std::cout << std::endl;
    }
    std::cout << "==============================" << std::endl;
}

//vypíše nepřátele v lokaci, ve které se hrdina nachází
void Game::printEnemies() {
    for (int i = 0; i < m_hero->getLocation()->getEnemies().size(); ++i) {
        m_hero->getLocation()->getEnemies().at(i)->printInfo();
    }
    m_hero->getLocation()->getBoss()->printInfo();
}

//
// Created by mrlat on 24.01.2021.
//

#include "LocationFactory.h"
#include <random>


Location* LocationFactory::getLocationHollywood(){
    std::vector<Paparazzi* > enemies;
    Boss* boss;
    auto ef = new EasyEnemyFactory();
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(1, 2); // define the range
    int pocetNepratel = distr(gen); // generate numbers
    for (int i = 0; i < pocetNepratel; ++i) {
        enemies.push_back(ef->getPaparazzi());
    }
    boss = ef->getBoss();
    delete ef;
    return new Location("Holywood", "Hollywood je rodiště všech úžasných filmů.",enemies, boss);
}

Location* LocationFactory::getLocationVenice(){
    std::vector<Paparazzi* > enemies;
    Boss* boss;
    auto ef = new EasyEnemyFactory();
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(2, 3); // define the range
    int pocetNepratel = distr(gen); // generate numbers
    for (int i = 0; i < pocetNepratel; ++i) {
        enemies.push_back(ef->getPaparazzi());
    }
    boss = ef->getBoss();
    delete ef;
    return new Location("Venice Beach",
             "Venice Beach, dříve obývaná kulturisty jako například Arnoldem Schwarzennegrem, dnes již domov pro bezdomovce a feťáky.",enemies, boss);
}

Location* LocationFactory::getLocationTheObservatory(){
    std::vector<Paparazzi* > enemies;
    Boss* boss;
    auto ef = new HardEnemyFactory();
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(3, 4); // define the range
    int pocetNepratel = distr(gen); // generate numbers
    for (int i = 0; i < pocetNepratel; ++i) {
        enemies.push_back(ef->getPaparazzi());
    }
    boss = ef->getBoss();
    delete ef;
    return new Location("The Observatory", "Observatoř, rozléhající se na kopci nad Hollywood Hills nabízí nádhérný výhled na celé Los Angeles",enemies, boss);
}
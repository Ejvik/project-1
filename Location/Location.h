//
// Created by mrlat on 12.12.2020.
//

#ifndef SEMESTRALNI_PRACE_LOCATION_H
#define SEMESTRALNI_PRACE_LOCATION_H

#include <iostream>
#include <vector>
#include "../Enemy/EnemyFactory.h"
#include "../Enemy/EasyEnemyFactory.h"
#include "../Enemy/HardEnemyFactory.h"

class Location {
protected:
    std::string m_name;
    std::string m_description;
    std::vector<Paparazzi* > m_enemies;
    Boss* m_boss;
public:
    Location(std::string name, std::string description, std::vector<Paparazzi* > enemies, Boss* boss);

    std::string getLocationName();

    std::string getLocationDescription();

    std::vector<Paparazzi* > getEnemies();

    Boss* getBoss();

    void addEnemy(Paparazzi* enemy);

    void addBoss(Boss* boss);
};


#endif //SEMESTRALNI_PRACE_LOCATION_H

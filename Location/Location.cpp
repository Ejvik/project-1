//
// Created by mrlat on 12.12.2020.
//

#include "Location.h"

Location::Location(std::string name, std::string description,std::vector<Paparazzi* > enemies, Boss* boss) {
    m_name = name;
    m_description = description;
    m_boss = boss;
    m_enemies = enemies;
}

std::string Location::getLocationName() {
    return m_name;
}

std::string Location::getLocationDescription() {
    return m_description;
}

std::vector<Paparazzi* > Location::getEnemies() {
    return m_enemies;
}

Boss* Location::getBoss() {
    return m_boss;
}

void Location::addEnemy(Paparazzi *enemy) {
    m_enemies.push_back(enemy);
}

void Location::addBoss(Boss *boss) {
    m_boss = boss;
}
//
// Created by mrlat on 24.01.2021.
//

#ifndef SEMESTRALNI_PRACE_LOCATIONFACTORY_H
#define SEMESTRALNI_PRACE_LOCATIONFACTORY_H
#include "Location.h"

class LocationFactory {
public:
    LocationFactory() {};

    ~LocationFactory() {};

    Location* getLocationHollywood();

    Location* getLocationVenice();

    Location* getLocationTheObservatory();

};


#endif //SEMESTRALNI_PRACE_LOCATIONFACTORY_H

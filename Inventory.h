//
// Created by tomas on 10. 12. 2020.
//

#ifndef SEMESTRALNI_PRACE_INVENTORY_H
#define SEMESTRALNI_PRACE_INVENTORY_H

#include <iostream>
#include "Weapon/Weapon.h"
#include "Armor/Armor.h"
#include "Items/ItemsFactory.h"
#include <vector>

class Inventory {
    std::vector<Weapon*> m_weapons;
    std::vector<Armor*> m_armors;
    std::vector<HealthPotion*> m_healthPotions;
    std::vector<StaminaPotion*> m_staminaPotions;
public:
    Inventory();

    std::vector<Weapon*> getWeapons();

    std::vector<Armor*> getArmors();

    std::vector<HealthPotion*> getHealthPotions();

    std::vector<StaminaPotion*> getStaminaPotions();

    void putHealthPotionIn(HealthPotion* potion);

    void putStaminaPotionIn(StaminaPotion* potion);

    void dropHealthPotion();

    void dropStaminaPotion();

    void putWeaponIn(Weapon* weapon);

    void putArmorIn(Armor* armor);

    void putWeaponOut(Weapon* weapon);

    void putArmorOut(Armor* armor);

    Armor* selectArmor();

    Weapon* selectWeapon();



};


#endif //SEMESTRALNI_PRACE_INVENTORY_H

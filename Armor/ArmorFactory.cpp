//
// Created by mrlat on 23.01.2021.
//

#include "ArmorFactory.h"
#include <random>

Armor* ArmorFactory::getArmorCardboard(){
    return new Armor("Kartonové brnění", 3, 2, "Když jsi socan, tak máš aspoň něco.");
}
Armor* ArmorFactory::getArmorPlastic(){
    return new Armor("Plastové brnění", 5, 3, "Furt jsi chudej, ale už je to o něco lepší.");
}
Armor* ArmorFactory::getArmorIron(){
    return new Armor("Železné brnění", 7, 5, "Železné brnění patří k průměru, máš dobrý základ. ");
}

Armor* ArmorFactory::getArmorVest() {
    return new Armor("Kevlarová vesta", 9, 5,"Kevlarová vesta");
}

Armor *ArmorFactory::getRandomArmor() {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(1, 100); // define the range
    int sance = distr(gen); // generate numbers
    switch (sance) {
        case 1 ... 30:
            std::cout<<"Obdržel si "<<getArmorCardboard()->getArmorName()<<std::endl;
            return getArmorCardboard();
        case 31 ... 60:
            std::cout<<"Obdržel si "<<getArmorPlastic()->getArmorName()<<std::endl;
            return getArmorPlastic();
        case 61 ... 80:
            std::cout<<"Obdržel si "<<getArmorIron()->getArmorName()<<std::endl;
            return getArmorIron();
        case 81 ... 100:
            std::cout<<"Obdržel si "<<getArmorVest()->getArmorName()<<std::endl;
            return getArmorVest();
    }
    return nullptr;
}
//
// Created by tomas on 10. 12. 2020.
//


#include <random>
#include "Armor.h"

Armor::Armor(std::string name, float bonusDef, float weight, std::string description) {
    m_name = name;
    m_bonusDefense = bonusDef;
    m_weight = weight;
    m_description = description;
}

void Armor::printInfo(){
    std::cout<<getArmorName()<<" | brnění: "<<getArmorBonusDefense()<<" | váha: "<<getArmorWeight();
}

std::string Armor::getArmorName() {
    return m_name;
}

float Armor::getArmorBonusDefense() {
    return m_bonusDefense;
}

float Armor::getArmorWeight() {
    return m_weight;
}

std::string Armor::getArmorDescription() {
    return m_description;
}
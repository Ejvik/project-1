//
// Created by mrlat on 23.01.2021.
//

#ifndef SEMESTRALNI_PRACE_ARMORFACTORY_H
#define SEMESTRALNI_PRACE_ARMORFACTORY_H
#include "Armor.h"

class ArmorFactory {
public:
    ArmorFactory() {};

    ~ArmorFactory() {};

    Armor* getArmorCardboard();

    Armor* getArmorPlastic();

    Armor* getArmorIron();

    Armor* getArmorVest();

    Armor* getRandomArmor();

};


#endif //SEMESTRALNI_PRACE_ARMORFACTORY_H

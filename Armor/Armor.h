//
// Created by tomas on 10. 12. 2020.
//

#ifndef SEMESTRALNI_PRACE_ARMOR_H
#define SEMESTRALNI_PRACE_ARMOR_H

#include <iostream>


class Armor {
protected:
    std::string m_name;
    float m_bonusDefense;
    float m_weight;
    std::string m_description;

public:
    Armor(std::string name, float bonusDef, float weight, std::string description);

    void printInfo();

    std::string getArmorName();

    float getArmorBonusDefense();

    float getArmorWeight();

    std::string getArmorDescription();

};


#endif //SEMESTRALNI_PRACE_ARMOR_H

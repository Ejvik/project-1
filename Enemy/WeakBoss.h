//
// Created by tomas on 11.01.2021.
//

#ifndef SEMESTRALNI_PRACE_WEAKBOSS_H
#define SEMESTRALNI_PRACE_WEAKBOSS_H

#include "Boss.h"

class WeakBoss : public Boss {

public:
    WeakBoss(std::string name, int strength, int health);

};


#endif //SEMESTRALNI_PRACE_WEAKBOSS_H

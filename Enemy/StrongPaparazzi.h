//
// Created by tomas on 11.01.2021.
//

#ifndef SEMESTRALNI_PRACE_STRONGPAPARAZZI_H
#define SEMESTRALNI_PRACE_STRONGPAPARAZZI_H

#include "Paparazzi.h"

class StrongPaparazzi : public Paparazzi {

public:
    StrongPaparazzi(std::string name, int strength, int health);

};


#endif //SEMESTRALNI_PRACE_STRONGPAPARAZZI_H

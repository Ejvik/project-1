//
// Created by tomas on 11.01.2021.
//

#ifndef SEMESTRALNI_PRACE_PAPARAZZI_H
#define SEMESTRALNI_PRACE_PAPARAZZI_H
#include <iostream>

class Paparazzi {
protected:
    std::string m_name;
    int m_strength;
    int m_health;
    bool m_dead = false;
public:
     void printInfo();

     std::string getName();

     int getAttack();

     int getHealth();

     void setHealth(int health);

     void lowerHealth(int num);

     bool getStatus();

     void setStatus(bool status);

    virtual ~Paparazzi() {};

    Paparazzi() {};
};


#endif //SEMESTRALNI_PRACE_PAPARAZZI_H

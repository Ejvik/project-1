//
// Created by tomas on 11.01.2021.
//

#include "HardEnemyFactory.h"
#include <random>

Boss *HardEnemyFactory::getBoss() {
    return new StrongBoss("Silný Boss",30, 300);
}

Paparazzi *HardEnemyFactory::getPaparazzi() {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(14, 16); // define the range
    int attack = distr(gen); // generate numbers
    return new StrongPaparazzi("Silný Paparazzi",attack, 150);
}
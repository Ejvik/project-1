//
// Created by tomas on 11.01.2021.
//

#include "Boss.h"

void Boss::printInfo(){
    std::cout<<m_name<<" | síla: "<<m_strength<<" | životy: "<<m_health<<std::endl;
}

std::string Boss::getName(){
    return m_name;
}

int Boss::getAttack() {
    return m_strength;
}

int Boss::getHealth() {
    return m_health;
}

void Boss::lowerHealth(int num) {
    m_health-=num;
}

bool Boss::getStatus(){
    return m_dead;
}

void Boss::setStatus(bool status){
    m_dead = status;
}
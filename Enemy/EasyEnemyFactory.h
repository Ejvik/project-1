//
// Created by tomas on 11.01.2021.
//

#ifndef SEMESTRALNI_PRACE_EASYENEMYFACTORY_H
#define SEMESTRALNI_PRACE_EASYENEMYFACTORY_H

#include "EnemyFactory.h"
#include "WeakPaparazzi.h"
#include "WeakBoss.h"

class EasyEnemyFactory : public EnemyFactory {
public:
    Boss *getBoss();

    Paparazzi *getPaparazzi();
};


#endif //SEMESTRALNI_PRACE_EASYENEMYFACTORY_H

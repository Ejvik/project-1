//
// Created by tomas on 11.01.2021.
//

#ifndef SEMESTRALNI_PRACE_ENEMYFACTORY_H
#define SEMESTRALNI_PRACE_ENEMYFACTORY_H

#include "Boss.h"
#include "Paparazzi.h"

class EnemyFactory {
public:
    virtual Boss *getBoss() = 0;

    virtual Paparazzi *getPaparazzi() = 0;
};


#endif //SEMESTRALNI_PRACE_ENEMYFACTORY_H

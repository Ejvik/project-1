//
// Created by tomas on 11.01.2021.
//

#include "Paparazzi.h"

void Paparazzi::printInfo(){
    std::cout<<m_name<<" | síla: "<<m_strength<<" | životy: "<<m_health<<std::endl;
}

std::string Paparazzi::getName(){
    return m_name;
}

int Paparazzi::getAttack() {
    return m_strength;
}

int Paparazzi::getHealth(){
    return m_health;
}

void Paparazzi::setHealth(int health) {
    m_health = health;
}
void Paparazzi::lowerHealth(int num){
    m_health-=num;
}

bool Paparazzi::getStatus() {
    return m_dead;
}

void Paparazzi::setStatus(bool status) {
    m_dead = status;
}
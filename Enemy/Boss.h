//
// Created by tomas on 11.01.2021.
//

#ifndef SEMESTRALNI_PRACE_BOSS_H
#define SEMESTRALNI_PRACE_BOSS_H
#include <iostream>

class Boss {
protected:
    std::string m_name;
    int m_strength;
    int m_health;
    bool m_dead = false;
public:
     void printInfo();

     std::string getName();

     int getAttack();

     int getHealth();

     bool getStatus();

     void setStatus(bool status);

     void lowerHealth(int num);

    Boss() {}

    virtual ~Boss() {};
};


#endif //SEMESTRALNI_PRACE_BOSS_H

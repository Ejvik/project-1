//
// Created by tomas on 11.01.2021.
//

#include "WeakBoss.h"

WeakBoss::WeakBoss(std::string name, int strength, int health) {
    m_name = name;
    m_strength = strength;
    m_health = health;
}

//
// Created by tomas on 11.01.2021.
//

#ifndef SEMESTRALNI_PRACE_WEAKPAPARAZZI_H
#define SEMESTRALNI_PRACE_WEAKPAPARAZZI_H

#include "Paparazzi.h"

class WeakPaparazzi : public Paparazzi {

public:
    WeakPaparazzi(std::string name, int strength, int health);

};


#endif //SEMESTRALNI_PRACE_WEAKPAPARAZZI_H

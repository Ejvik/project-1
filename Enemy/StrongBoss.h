//
// Created by tomas on 11.01.2021.
//

#ifndef SEMESTRALNI_PRACE_STRONGBOSS_H
#define SEMESTRALNI_PRACE_STRONGBOSS_H

#include "Boss.h"

class StrongBoss : public Boss {

public:
    StrongBoss(std::string name, int strength, int health);

};


#endif //SEMESTRALNI_PRACE_STRONGBOSS_H

//
// Created by tomas on 11.01.2021.
//

#include "EasyEnemyFactory.h"
#include <random>

Boss *EasyEnemyFactory::getBoss() {
    return new WeakBoss("Slabý Boss",20, 200);
}

Paparazzi *EasyEnemyFactory::getPaparazzi() {
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(8, 12); // define the range
    int attack = distr(gen); // generate numbers
    return new WeakPaparazzi("Slabý Paparazzi",attack, 100);
}
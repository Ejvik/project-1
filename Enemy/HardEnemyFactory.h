//
// Created by tomas on 11.01.2021.
//

#ifndef SEMESTRALNI_PRACE_HARDENEMYFACTORY_H
#define SEMESTRALNI_PRACE_HARDENEMYFACTORY_H

#include "EnemyFactory.h"
#include "StrongBoss.h"
#include "StrongPaparazzi.h"

class HardEnemyFactory : public EnemyFactory {
public:
    Boss *getBoss();

    Paparazzi *getPaparazzi();
};


#endif //SEMESTRALNI_PRACE_HARDENEMYFACTORY_H

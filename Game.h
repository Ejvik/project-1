//
// Created by tomas on 15. 12. 2020.
//

#ifndef SEMESTRALNI_PRACE_GAME_H
#define SEMESTRALNI_PRACE_GAME_H

#include <iostream>
#include <vector>
#include "Hero.h"
#include "Location/LocationFactory.h"
#include "Weapon/WeaponFactory.h"
#include "Armor/ArmorFactory.h"
#include "Inventory.h"
#include "Enemy/EasyEnemyFactory.h"
#include "Enemy/HardEnemyFactory.h"
#include "Interaction/Fight.h"


class Game {
    Hero *m_hero;
    std::vector<Location *> m_locations;
public:
    Game();

    int HeroOptions();

    void option1();

    void option2();

    void option3();

    void option4();

    void option5();

    void option6();

    void option7();

    void inventoryOptions();

    void changeLocation();

    void printMap();

    void setLocations();

    void printEnemies();

    int endGame();
};


#endif //SEMESTRALNI_PRACE_GAME_H

//
// Created by mrlat on 10.12.2020.
//

#include "Hero.h"
#include "Interaction/Interaction.h"

/* Hero
 * Konstruktor hrdiny a jeho vytvoření
 * Vybrání jména a klanu
 */

Hero::Hero() {
    auto classFactory = new ClassFactory();
    char name[100];
    std::cout << "Zadej jméno svého hrdiny: " << std::endl;
    std::cout << "> ";
    std::cin.getline(name, sizeof(name));
    std::cout << std::endl;
    m_name = name;
    m_class = classFactory->chooseClass();
    m_inventory = new Inventory;
    setStartingEquipment();
    delete classFactory;
}

/* printInfo a printEquipment
 * Vypsání informací o hrdinovi
 * Vypsání informací o jeho vybavení
 */

void Hero::setStartingEquipment(){
    auto weaponFactory = new WeaponFactory();
    auto armorFactory = new ArmorFactory();
    m_inventory->putWeaponIn(weaponFactory->getWeaponScissors());
    m_inventory->putArmorIn(armorFactory->getArmorCardboard());
    m_weapon = m_inventory->getWeapons().at(0);
    m_armor = m_inventory->getArmors().at(0);
    addHealthPotion();
    addStaminaPotion();
    delete weaponFactory;
    delete armorFactory;
}

void Hero::setStats() {
    m_dexterity = m_class->getDexterity();
    m_description = m_class->getDescription();
    m_clanname = m_class->getName();
    m_stamina = m_class->getStamina()*30;
    m_strength = m_class->getStrength()*2;
    m_health = 100;
}


void Hero::printInfo() {
    std::cout << "Jméno hrdiny: " << getHeroName() << std::endl;
    std::cout << "Klan: " << getClanName() << std::endl;
    std::cout << "Síla: " << getStrength() << std::endl;
    std::cout << "Obratnost: " << getDexterity() << std::endl;
    std::cout << "Výdrž: " << getStamina() << std::endl;
    std::cout << "Životy: " << getHeroHealth() << std::endl;
    std::cout << "Lokace: " << getLocation()->getLocationName() << std::endl;
}

void Hero::printEquipment() {
    std::cout << "Název brnění: " << getArmor()->getArmorName() << std::endl;
    std::cout << "Obrana brnění: " << getArmor()->getArmorBonusDefense() << std::endl;
    std::cout << "Váha brnění: " << getArmor()->getArmorWeight() << std::endl;
    std::cout << "Popis brnění: " << getArmor()->getArmorDescription() << std::endl << std::endl;

    std::cout << "Název zbraně: " << getWeapon()->getWeaponName() << std::endl;
    std::cout << "Útok zbraně: " << getWeapon()->getWeaponBonusAttack() << std::endl;
    std::cout << "Váha zbraně: " << getWeapon()->getWeaponWeight() << std::endl;
    std::cout << "Popis zbraně: " << getWeapon()->getWeaponDescription() << std::endl;
}

/* metody get/set
 * Metody pro získání určitých informací o hrdinovi pro další zpracování
 * Metody pro nastavení určitých informací hrdinovi, např. zbraň, brnění, atd.
 */
std::string Hero::getHeroName() {
    return m_name;
}

std::string Hero::getClanName() {
    return m_clanname;
}

int Hero::getHeroHealth() {
    return m_health;
}

int Hero::getStrength() {
    return m_strength;
}

int Hero::getDexterity() {
    return m_dexterity;
}

int Hero::getStamina() {
    return m_stamina;
}

std::string Hero::getDescription() {
    return m_description;
}

int Hero::getDecision() {
    int decision = 0;
    std::cout << "==============================" << std::endl;
    std::cout << "Zadej číslo interakce, kterou si žádáš provést: " << std::endl;
    std::cout << "==============================" << std::endl;
    printInteraction();
    std::cout << "> ";
    std::cin >> decision;
    std::cout<<std::endl;
    if (decision < m_interaction.size()) {
        return decision;
    } else {
        std::cout << "==============================" << std::endl;
        std::cout << "Zadal si číslo, které nebylo na seznamu." << std::endl;
        std::cout << "==============================" << std::endl;
        printInteraction();
        getDecision();
    }
    return 0;
}

void Hero::printInteraction() {
    for (int i = 0; i < m_interaction.size(); ++i) {
        std::cout << "[" << i << "]" << m_interaction.at(i)->getDescription() << std::endl;
    }
}

void Hero::learnInteraction(Interaction *interaction) {
    m_interaction.push_back(interaction);
}

void Hero::interactEnemy(Paparazzi *with) {
    int decision = getDecision();
    m_interaction.at(decision)->interactEnemy(this, with);
}

void Hero::interactBoss(Boss *with) {
    int decision = getDecision();
    m_interaction.at(decision)->interactBoss(this, with);
}

void Hero::printInventory() {
    std::cout << "==============================" << std::endl;
    std::cout << "Zbraně: " << std::endl;
    for (int i = 0; i < m_inventory->getWeapons().size(); ++i) {
        m_inventory->getWeapons().at(i)->printInfo();
        if (m_inventory->getWeapons().at(i) == getWeapon())
            std::cout << " << tohle používáš" << std::endl;
        else std::cout << std::endl;
    }
    std::cout << "Brnění: " << std::endl;
    for (int i = 0; i < m_inventory->getArmors().size(); ++i) {
        m_inventory->getArmors().at(i)->printInfo();
        if (m_inventory->getArmors().at(i) == getArmor())
            std::cout << " << tohle používáš" << std::endl;
        else std::cout << std::endl;
    }
    std::cout << "Nápoje: " << std::endl;
        std::cout<< "Léčivý nápoj | bonus zdraví: 50 | počet: "<<m_inventory->getHealthPotions().size()<<std::endl;
        std::cout<< "Redbull | bonus výdrže: 20 | počet: "<<m_inventory->getStaminaPotions().size()<<std::endl;
    std::cout<<"=============================="<<std::endl;
}

void Hero::equipWeapon(Weapon *weapon) {
    m_weapon = weapon;
}

Weapon *Hero::getWeapon() {
    return m_weapon;
}

void Hero::equipArmor(Armor *armor) {
    m_armor = armor;
}

Armor *Hero::getArmor() {
    return m_armor;
}

void Hero::lowerHealth(int num) {
    m_health -= num;
}

void Hero::addHealth(int num){
    m_health+=num;
}

void Hero::lowerStamina(int num){
    m_stamina-=num;
}

void Hero::addStamina(int num){
    m_stamina+=num;
}

void Hero::setStamina(int num){
    m_stamina = num;
}

void Hero::setLocation(Location *location) {
    m_location = location;
}

Location *Hero::getLocation() {
    return m_location;
}


int Hero::getMapPosition() {
    return m_mapPosition;
}

void Hero::setMapPosition(int change) {
    m_mapPosition += change;
}

Inventory *Hero::getInventory() {
    return m_inventory;
}

void Hero::useHealthPotion(){
    if (m_inventory->getHealthPotions().size()>0){
        addHealth(m_inventory->getHealthPotions().at(0)->getHealthBonus());
        m_inventory->dropHealthPotion();
        std::cout<<"Použil si jeden léčivý nápoj"<<std::endl;
        std::cout<<"Tvoje životy se zvýšily na: "<<getHeroHealth()<<std::endl<<std::endl;
    }else{
        std::cout<<"Nemáš žádný léčivý nápoj"<<std::endl<<std::endl;
    }
}

void Hero::useStaminaPotion(){
    if (m_inventory->getStaminaPotions().size()>0){
        addStamina(m_inventory->getStaminaPotions().at(0)->getStaminaBonus());
        m_inventory->dropStaminaPotion();
        std::cout<<"Použil si jeden Redbull"<<std::endl;
        std::cout<<"Tvoje výdrž se zvýšila na: "<<getStamina()<<std::endl<<std::endl;
    }else{
        std::cout<<"Nemáš žádný RedBull"<<std::endl<<std::endl;
    }
}

void Hero::addHealthPotion(){
    auto potionFactory = new ItemsFactory();
    m_inventory->putHealthPotionIn(potionFactory->getHealthPotion());
    delete potionFactory;
    std::cout<<"Obdržel si jeden léčivý nápoj"<<std::endl;
}

void Hero::addStaminaPotion() {
    auto potionFactory = new ItemsFactory();
    m_inventory->putStaminaPotionIn(potionFactory->getStaminaPotion());
    delete potionFactory;
    std::cout<<"Obdržel si jeden RedBull"<<std::endl;
}

int Hero::getKeys(){
    return m_keys;
}

void Hero::addKey(){
    m_keys++;
}

void Hero::printFigthInfo() {
    std::cout<<"Hrdina"<<std::endl;
    std::cout<<"Zdraví: "<<getHeroHealth()<<std::endl;
    std::cout<<"Výdrž: "<<getStamina()<<std::endl;
}
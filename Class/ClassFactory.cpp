//
// Created by mrlat on 24.01.2021.
//

#include "ClassFactory.h"

Class* ClassFactory::getClassSandler(){
    return new Class("Sandleři", 4, 3, 3,
                     "Sandleři jsou jeden z nejmocnějších klanů v galaxii. Hlavni představitel klanu je Adam Sandler.");
}
Class* ClassFactory::getClassFraser(){
    return new Class("Fraseři", 2, 4, 4,
                     "Fraseři jsou klan, který je hlavně známý lovem mumií. Hlavní představitel klanu je Brendan Fraser.");
}
Class* ClassFactory::getClassRogen(){
    return new Class("Rogeni", 6, 2, 2,
                     "Klan Roganů je převážně známý svým důvtipem a zápalem pro boj. Hlavním představitelem klanu je Seth Rogen.");
}
Class* ClassFactory::chooseClass(){
    char options;
    std::cout << "Nejprve si vyberte klan, do které vaše postava bude patřit. " << std::endl;
    std::cout << "Podle výběru klanu, se vaší postavě přidělí statistiky." << std::endl<<std::endl;
    std::cout << "==============================" << std::endl;
    getClassSandler()->printInfo();
    std::cout << "==============================" << std::endl;
    getClassFraser()->printInfo();
    std::cout << "==============================" << std::endl;
    getClassRogen()->printInfo();
    std::cout << "==============================" << std::endl;
    std::cout << "Vyberte klan" << std::endl;
    std::cout << "[s]andleři  [f]raseři  [r]ogeni" << std::endl;
    std::cout << "> ";
    std::cin >> options;
    std::cout << "==============================" << std::endl;
    switch (options) {
        case 's':
            return getClassSandler();
        case 'f':
            return getClassFraser();
        case 'r':
            return getClassRogen();
        default:
            return getClassSandler();
    }
}
//
// Created by mrlat on 24.01.2021.
//

#ifndef SEMESTRALNI_PRACE_CLASSFACTORY_H
#define SEMESTRALNI_PRACE_CLASSFACTORY_H
#include "Class.h"

class ClassFactory {
public:
    ClassFactory() {};

    ~ClassFactory() {};

    Class* getClassSandler();

    Class* getClassFraser();

    Class* getClassRogen();

    Class* chooseClass();

};


#endif //SEMESTRALNI_PRACE_CLASSFACTORY_H

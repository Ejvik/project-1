//
// Created by tomas on 10. 12. 2020.
//

#ifndef SEMESTRALNI_PRACE_CLASS_H
#define SEMESTRALNI_PRACE_CLASS_H

#include <iostream>
#include <vector>


class Class {
protected:
    std::string m_name;
    int m_strength;
    int m_dexterity;
    int m_stamina;
    std::string m_description;
public:
    Class(std::string name, int strength, int dexterity, int stamina, std::string description);

    void printInfo();

    std::string getName() const;

    int getStrength() const;

    int getDexterity() const;

    int getStamina() const;

    std::string getDescription();

};


#endif //SEMESTRALNI_PRACE_CLASS_H

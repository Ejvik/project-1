//
// Created by tomas on 10. 12. 2020.
//

#include "Class.h"

Class::Class(std::string name, int strength, int dexterity, int stamina, std::string description) {
    m_name = name;
    m_strength = strength;
    m_dexterity = dexterity;
    m_stamina = stamina;
    m_description = description;
}

void Class::printInfo() {
    std::cout << m_name << std::endl << std::endl;
    std::cout << m_description << std::endl;
    std::cout << "Síla: " << m_strength << std::endl;
    std::cout << "Obratnost : " << m_dexterity << std::endl;
    std::cout << "Výdrž: " << m_stamina << std::endl;
}



std::string Class::getName() const{
    return m_name;
}

int Class::getStrength() const{
    return m_strength;
}

int Class::getDexterity() const{
    return m_dexterity;
}

int Class::getStamina() const{
    return m_stamina;
}

std::string Class::getDescription() {
    return m_description;
}
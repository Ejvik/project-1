//
// Created by mrlat on 23.01.2021.
//

#ifndef SEMESTRALNI_PRACE_WEAPONFACTORY_H
#define SEMESTRALNI_PRACE_WEAPONFACTORY_H
#include "Weapon.h"

class WeaponFactory {
public:
    WeaponFactory() {};

    ~WeaponFactory() {};

    Weapon *getWeaponScissors();

    Weapon* getWeaponHammer();

    Weapon* getWeaponShovel();

    Weapon* getWeaponM4();

    Weapon* getRandomWeapon();

};


#endif //SEMESTRALNI_PRACE_WEAPONFACTORY_H

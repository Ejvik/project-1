//
// Created by mrlat on 23.01.2021.
//

#include "WeaponFactory.h"
#include <random>

Weapon *WeaponFactory::getWeaponScissors(){
    return new Weapon("Nůžky", 15, 2, "Velice nebezpečná zbraň se kterou by se nemělo běhat.");
}

Weapon* WeaponFactory::getWeaponHammer(){
    return new Weapon("Kladivo", 20, 4, "Když potřebujete rozbít tvrdý oříšek.");
}

Weapon* WeaponFactory::getWeaponShovel(){
    return new Weapon("Lopata", 30, 6, "Tupý předmět na tupé .");
}

Weapon* WeaponFactory::getWeaponM4() {
    return new Weapon("M4", 50, 10, "Útočná puška M4, ideální na ustřelení nějakých hlav");
}

Weapon* WeaponFactory::getRandomWeapon(){
    std::random_device rd; // obtain a random number from hardware
    std::mt19937 gen(rd()); // seed the generator
    std::uniform_int_distribution<> distr(1, 100); // define the range
    int sance = distr(gen); // generate numbers
    switch (sance) {
        case 1 ... 40:
            std::cout<<"Obdržel si "<<getWeaponScissors()->getWeaponName()<<std::endl;
            return getWeaponScissors();
        case 41 ... 60:
            std::cout<<"Obdržel si "<<getWeaponHammer()->getWeaponName()<<std::endl;
            return getWeaponHammer();
        case 61 ... 80:
            std::cout<<"Obdržel si "<<getWeaponShovel()->getWeaponName()<<std::endl;
            return getWeaponShovel();
        case 81 ... 100 :
            std::cout<<"Obdržel si "<<getWeaponM4()->getWeaponName()<<std::endl;
            return getWeaponM4();
    }
    return nullptr;
}
//
// Created by tomas on 10. 12. 2020.
//

#include <random>
#include "Weapon.h"

Weapon::Weapon(std::string name, float bonusAtt, float weight, std::string description) {
    m_name = name;
    m_bonusAttack = bonusAtt;
    m_weight = weight;
    m_description = description;
}

void Weapon::printInfo() {
    std::cout<<getWeaponName()<<" | útok: "<<getWeaponBonusAttack()<<" | váha: "<<getWeaponWeight();
}

std::string Weapon::getWeaponName() {
    return m_name;
}

float Weapon::getWeaponBonusAttack() {
    return m_bonusAttack;
}

float Weapon::getWeaponWeight() {
    return m_weight;
}

std::string Weapon::getWeaponDescription() {
    return m_description;
}

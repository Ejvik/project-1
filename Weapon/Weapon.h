//
// Created by tomas on 10. 12. 2020.
//

#ifndef SEMESTRALNI_PRACE_WEAPON_H
#define SEMESTRALNI_PRACE_WEAPON_H

#include <iostream>

class Weapon {
protected:
    std::string m_name;
    float m_bonusAttack;
    float m_weight;
    std::string m_description;
public:
    Weapon(std::string name, float bonusAtt, float weight, std::string description);

    void printInfo();

    std::string getWeaponName();

    float getWeaponBonusAttack();

    float getWeaponWeight();

    std::string getWeaponDescription();
};


#endif //SEMESTRALNI_PRACE_WEAPON_H

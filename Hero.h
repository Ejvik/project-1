//
// Created by mrlat on 10.12.2020.
//

#ifndef SEMESTRALNI_PRACE_HERO_H
#define SEMESTRALNI_PRACE_HERO_H

#include <iostream>
#include <vector>
#include "Armor/ArmorFactory.h"
#include "Weapon/WeaponFactory.h"
#include "Location/Location.h"
#include "Inventory.h"
#include "Class/ClassFactory.h"

class Interaction;

class Hero {
    std::vector<Interaction *> m_interaction;
    Weapon *m_weapon;
    Armor *m_armor;
    Location *m_location;
    Inventory *m_inventory;
    int m_mapPosition = 0;
    Class *m_class;
    std::string m_name;
    std::string m_clanname;
    int m_health;
    int m_strength;
    int m_dexterity;
    int m_stamina;
    std::string m_description;
    int m_keys = 0;



public:
    Hero();

    void setStartingEquipment();

    void setStats();

    void printInfo();

    void printEquipment();

    void printInventory();

    std::string getHeroName();

    std::string getClanName();

    int getHeroHealth();

    int getStrength();

    int getDexterity();

    int getStamina();

    std::string getDescription();

    void lowerHealth(int num);

    void addHealth(int num);

    void lowerStamina(int num);

    void addStamina(int num);

    void setStamina(int num);

    void learnInteraction(Interaction *interaction);

    void interactEnemy(Paparazzi* with);

    void interactBoss(Boss* with);

    void equipWeapon(Weapon* weapon);

    Weapon *getWeapon();

    void equipArmor(Armor* armor);

    Armor *getArmor();

    void setLocation(Location *location);

    Location *getLocation();

    int getMapPosition();

    void setMapPosition(int change);

    Inventory* getInventory();

    void useHealthPotion();

    void useStaminaPotion();

    void addHealthPotion();

    void addStaminaPotion();

    int getKeys();

    void addKey();

    void printFigthInfo();

private:
    void printInteraction();

    int getDecision();
};


#endif //SEMESTRALNI_PRACE_HERO_H

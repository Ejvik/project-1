//
// Created by mrlat on 23.01.2021.
//

#ifndef SEMESTRALNI_PRACE_ITEMSFACTORY_H
#define SEMESTRALNI_PRACE_ITEMSFACTORY_H
#include "HealthPotion.h"
#include "StaminaPotion.h"

class ItemsFactory {
public:
    ItemsFactory() {};

    ~ItemsFactory() {};

    HealthPotion* getHealthPotion();

    StaminaPotion* getStaminaPotion();

};


#endif //SEMESTRALNI_PRACE_ITEMSFACTORY_H

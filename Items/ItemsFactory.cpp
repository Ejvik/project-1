//
// Created by mrlat on 23.01.2021.
//

#include "ItemsFactory.h"

HealthPotion* ItemsFactory::getHealthPotion(){
    return new HealthPotion("Léčivý nápoj", 50);
}

StaminaPotion* ItemsFactory::getStaminaPotion(){
    return new StaminaPotion("RedBull", 20);
}
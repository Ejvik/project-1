//
// Created by mrlat on 23.01.2021.
//

#ifndef SEMESTRALNI_PRACE_STAMINAPOTION_H
#define SEMESTRALNI_PRACE_STAMINAPOTION_H
#include <iostream>

class StaminaPotion {
    std::string m_name;
    int m_staminaBonus;
public:
    StaminaPotion(std::string name, int staminaBonus);

    ~StaminaPotion() {};

    std::string getName();

    int getStaminaBonus();
};


#endif //SEMESTRALNI_PRACE_STAMINAPOTION_H

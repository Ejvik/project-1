//
// Created by mrlat on 23.01.2021.
//

#ifndef SEMESTRALNI_PRACE_HEALTHPOTION_H
#define SEMESTRALNI_PRACE_HEALTHPOTION_H
#include <iostream>

class HealthPotion {
    std::string m_name;
    int m_healthBonus;
public:
    HealthPotion(std::string name, int healthBonus);

    ~HealthPotion() {};

    std::string getName();

    int getHealthBonus();

};


#endif //SEMESTRALNI_PRACE_HEALTHPOTION_H

//
// Created by mrlat on 23.01.2021.
//

#include "HealthPotion.h"

HealthPotion::HealthPotion(std::string name, int healthBonus) {
    m_name = name;
    m_healthBonus = healthBonus;
}

std::string HealthPotion::getName(){
    return m_name;
}

int HealthPotion::getHealthBonus(){
    return m_healthBonus;
}
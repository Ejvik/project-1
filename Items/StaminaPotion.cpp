//
// Created by mrlat on 23.01.2021.
//

#include "StaminaPotion.h"

StaminaPotion::StaminaPotion(std::string name, int staminaBonus){
    m_name = name;
    m_staminaBonus = staminaBonus;
}

std::string StaminaPotion::getName(){
    return m_name;
}

int StaminaPotion::getStaminaBonus(){
    return m_staminaBonus;
}
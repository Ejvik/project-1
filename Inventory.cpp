//
// Created by tomas on 10. 12. 2020.
//

#include "Inventory.h"

Inventory::Inventory() {

}

std::vector<Weapon *> Inventory::getWeapons() {
    return m_weapons;
}

std::vector<Armor *> Inventory::getArmors() {
    return m_armors;
}

std::vector<HealthPotion*> Inventory::getHealthPotions(){
    return m_healthPotions;
}

std::vector<StaminaPotion*> Inventory::getStaminaPotions(){
    return m_staminaPotions;
}

void Inventory::putHealthPotionIn(HealthPotion* potion){
    m_healthPotions.push_back(potion);
}

void Inventory::putStaminaPotionIn(StaminaPotion* potion){
    m_staminaPotions.push_back(potion);
}

void Inventory::dropHealthPotion() {
    m_healthPotions.pop_back();
}

void Inventory::dropStaminaPotion() {
    m_staminaPotions.pop_back();
}

void Inventory::putWeaponIn(Weapon *weapon) {
    m_weapons.push_back(weapon);
}

void Inventory::putArmorIn(Armor *armor) {
    m_armors.push_back(armor);
}

void Inventory::putWeaponOut(Weapon* weapon){
    for (int i = 0; i < m_weapons.size(); ++i) {
        if (m_weapons.at(i) == weapon)
            m_weapons.erase(m_weapons.begin()+i);
    }
}

void Inventory::putArmorOut(Armor* armor){
    for (int i = 0; i < m_armors.size(); ++i) {
        if (m_armors.at(i) == armor)
            m_armors.erase(m_armors.begin()+i);
    }
}

Weapon *Inventory::selectWeapon() {
    std::cout << "Kterou zbraň chceš nasadit? " << std::endl;
    for (int i = 0; i < m_weapons.size(); ++i) {
        std::cout << "[" << i + 1 << "] ";
        m_weapons.at(i)->printInfo();
        std::cout << std::endl;
    }
    char options;
    std::cout << "> ";
    std::cin >> options;
    for (int i = 0; i < m_weapons.size(); ++i) {
        if (options - 48 == i + 1) {
            return m_weapons.at(i);
        }
    }
    return 0;
}

Armor *Inventory::selectArmor() {
    std::cout << "Které brnění chceš nasadit? " << std::endl;
    for (int i = 0; i < m_armors.size(); ++i) {
        std::cout << "[" << i + 1 << "] ";
        m_armors.at(i)->printInfo();
        std::cout << std::endl;
    }
    char options;
    std::cout << "> ";
    std::cin >> options;
    for (int i = 0; i < m_armors.size(); ++i) {
        if (options - 48 == i + 1) {
            return m_armors.at(i);
        }
    }
    return 0;
}


//
// Created by tomas on 14.12.2020.
//

#include "Interaction.h"


Interaction::Interaction(std::string description) {
    m_description = description;
}

std::string Interaction::getDescription() {
    return m_description;
}
//
// Created by tomas on 14.12.2020.
//

#ifndef SEMESTRALNI_PRACE_INTERACTION_H
#define SEMESTRALNI_PRACE_INTERACTION_H

#include <iostream>
#include "../Hero.h"
#include "../Enemy/EnemyFactory.h"


class Interaction {
protected:
    std::string m_description;
public:
    Interaction(std::string description);

    virtual void interactEnemy(Hero *who, Paparazzi *with) = 0;

    virtual void interactBoss(Hero *who, Boss *with) = 0;

    std::string getDescription();
};


#endif //SEMESTRALNI_PRACE_INTERACTION_H

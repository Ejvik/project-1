//
// Created by tomas on 20.01.2021.
//

#include "Fight.h"
#include <stdlib.h>
Fight::Fight() : Interaction("Boj") {

}

void Fight::interactEnemy(Hero *who, Paparazzi *with) {
        int dmgHero = who->getWeapon()->getWeaponBonusAttack() + who->getStrength();
        int weightHero = who->getWeapon()->getWeaponWeight() + who->getArmor()->getArmorWeight();
        int dmgEnemy = with->getAttack();
        int armorHero = who->getArmor()->getArmorBonusDefense() + who->getDexterity();
        char option;
        if (with->getHealth() > 0) {
            std::cout << "Možnost:" << std::endl;
            std::cout << "[1] Zaútoč" << std::endl;
            std::cout << "[2] Použij léčivý nápoj" << std::endl;
            std::cout << "[3] Použij RedBull" << std::endl;
            std::cout << "[4] Informace o hrdinovi a protivníkovi" << std::endl;
            std::cout << "> ";
            std::cin >> option;
            switch (option) {
                case '1':
                    with->lowerHealth(dmgHero);
                    if (who->getStamina() < 0) {
                        who->lowerHealth(dmgEnemy - (who->getArmor()->getArmorBonusDefense()));
                    } else {
                        who->lowerHealth(dmgEnemy - armorHero);
                        if (who->getStamina() >= weightHero) {
                            who->lowerStamina(weightHero);
                        } else {
                            who->setStamina(0);
                        }
                    }
                    if (who->getHeroHealth() <= 0) {
                        std::cout << "Umřel si. Hra skončila." << std::endl;
                        abort();
                    }
                    std::cout << "Nepříteli si ubral: " << dmgHero << " životů" << std::endl;
                    std::cout << "Přišel si o: " << (dmgEnemy - armorHero) << " životů" << std::endl;
                    std::cout << "Přišel si o: " << weightHero << " staminy" << std::endl;
                    who->printFigthInfo();
                    with->printInfo();
                    std::cout << std::endl;
                    interactEnemy(who, with);
                    break;
                case '2':
                    who->useHealthPotion();
                    interactEnemy(who, with);
                    break;
                case '3':
                    who->useStaminaPotion();
                    interactEnemy(who, with);
                    break;
                case '4':
                    who->printInfo();
                    who->printInventory();
                    with->printInfo();
                    interactEnemy(who, with);
                    break;
                default:
                    std::cout << "Zadal si možnost, která není na seznamu" << std::endl;

            }
        } else {
            std::cout << "Porazil si nepřítele." << std::endl;
            for (int i = 0; i < who->getLocation()->getEnemies().size(); ++i) {
                if (who->getLocation()->getEnemies().at(i) == with) {
                    who->getLocation()->getEnemies().at(i)->setStatus(true);
                }
            }
            dropItems(who,1);
        }
    }

void Fight::interactBoss(Hero *who, Boss *with) {
    int dmgHero = who->getWeapon()->getWeaponBonusAttack() + who->getStrength();
    int weightHero = who->getWeapon()->getWeaponWeight() + who->getArmor()->getArmorWeight();
    int dmgEnemy = with->getAttack();
    int armorHero = who->getArmor()->getArmorBonusDefense() + who->getDexterity();
    char option;
    if (with->getHealth() > 0) {
        std::cout << "Možnost:" << std::endl;
        std::cout << "[1] Zaútoč" << std::endl;
        std::cout << "[2] Použij léčivý nápoj" << std::endl;
        std::cout << "[3] Použij RedBull" << std::endl;
        std::cout << "[4] Informace o hrdinovi a protivníkovi" << std::endl;
        std::cout << "> ";
        std::cin >> option;
        switch (option) {
            case '1':
                with->lowerHealth(dmgHero);
                if (who->getStamina() < 0) {
                    who->lowerHealth(dmgEnemy - (who->getArmor()->getArmorBonusDefense()));
                } else {
                    who->lowerHealth(dmgEnemy - armorHero);
                    if (who->getStamina() >= weightHero) {
                        who->lowerStamina(weightHero);
                    } else {
                        who->setStamina(0);
                    }
                }
                if (who->getHeroHealth() <= 0) {
                    std::cout << "Umřel si. Hra skončila." << std::endl;
                    abort();
                }
                std::cout << "Nepříteli si ubral: " << dmgHero << " životů" << std::endl;
                std::cout << "Přišel si o: " << (dmgEnemy - armorHero) << " životů" << std::endl;
                std::cout << "Přišel si o: " << weightHero << " staminy" << std::endl;
                who->printFigthInfo();
                with->printInfo();
                std::cout << std::endl;
                interactBoss(who, with);
                break;
            case '2':
                who->useHealthPotion();
                interactBoss(who, with);
                break;
            case '3':
                who->useStaminaPotion();
                interactBoss(who, with);
                break;
            case '4':
                who->printInfo();
                who->printInventory();
                with->printInfo();
                interactBoss(who, with);
                break;
            default:
                std::cout << "Zadal si možnost, která není na seznamu" << std::endl;

        }
    } else {
        std::cout << "Porazil si nepřítele." << std::endl;
        who->addKey();
            if (who->getLocation()->getBoss() == with) {
                who->getLocation()->getBoss()->setStatus(true);
            }
        if (who->getKeys()==3){
            std::cout<<"WINNER WINNER CHICHEN DINNER"<<std::endl<<"ggwp"<<std::endl;
            exit(0);
        }
        dropItems(who,2);
        }
    }

    void Fight::dropItems(Hero* who, int howMany) {
    int numberOf = howMany;
        auto weaponFactory = new WeaponFactory();
        auto armorFactory = new ArmorFactory();
        for (int i = 0; i < numberOf; ++i) {
            who->addStaminaPotion();
            who->addHealthPotion();
        }
        who->getInventory()->putWeaponIn(weaponFactory->getRandomWeapon());
        who->getInventory()->putArmorIn(armorFactory->getRandomArmor());
        delete weaponFactory;
        delete armorFactory;
}

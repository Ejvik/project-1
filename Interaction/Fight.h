//
// Created by tomas on 20.01.2021.
//

#ifndef SEMESTRALNI_PRACE_FIGHT_H
#define SEMESTRALNI_PRACE_FIGHT_H

#include "Interaction.h"

class Fight : public Interaction {
public:
    Fight();

    void interactEnemy(Hero *who, Paparazzi *with);

    void interactBoss(Hero *who, Boss *with);

    void dropItems(Hero *who, int howMany);
};


#endif //SEMESTRALNI_PRACE_FIGHT_H
